#!/usr/bin/env python
#
# Copyright (c) 2010 Christian Hergert <chris@dronelabs.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from mako.lookup import TemplateLookup
import os
import StringIO
import sys
import time

LOOKUP_PATHS = [os.path.join(os.path.dirname(__file__), 'templates')]
lookup = TemplateLookup(LOOKUP_PATHS)

def templateFor(name):
    if not name.endswith('.tmpl'):
        name += '.tmpl'
    return lookup.get_template(name)

def clientNameFor(tree, name, outdir='.'):
    prefix = tree.clientPrefix.lower()
    return os.path.join(outdir, prefix + '-' + name)

def serverNameFor(tree, name, outdir='.'):
    prefix = tree.serverPrefix.lower()
    return os.path.join(outdir, prefix + '-' + name)

def writeTemplate(filename, template, tree, client=True, **kwargs):
    d = template.render(Project=tree,
                        FileName=os.path.split(filename)[-1],
                        Copyright=tree.copyright, **kwargs)
    file(filename, 'w').write(d.strip())

def genClient(tree, outdir='.'):
    buildClientConn(tree)

    template = templateFor('client.connection.h')
    filename = clientNameFor(tree, 'connection.h', outdir=outdir)
    writeTemplate(filename, template, tree)

    template = templateFor('client.connection.c')
    filename = clientNameFor(tree, 'connection.c', outdir=outdir)
    writeTemplate(filename, template, tree)

    template = templateFor('client.connection-lowlevel.h')
    filename = clientNameFor(tree, 'connection-lowlevel.h', outdir=outdir)
    writeTemplate(filename, template, tree)

def genClientDBus(tree, outdir='.'):
    template = templateFor('client.dbus.h')
    filename = clientNameFor(tree, 'connection-dbus.h', outdir=outdir)
    writeTemplate(filename, template, tree)

    template = templateFor('client.dbus.c')
    filename = clientNameFor(tree, 'connection-dbus.c', outdir=outdir)
    writeTemplate(filename, template, tree)

def genServerDBus(tree, outdir='.'):
    template = templateFor('listener-dbus.h')
    filename = serverNameFor(tree, 'listener-dbus.h', outdir=outdir)
    writeTemplate(filename, template, tree)

    template = templateFor('listener-dbus.c')
    filename = serverNameFor(tree, 'listener-dbus.c', outdir=outdir)
    writeTemplate(filename, template, tree)

def genServerTests(tree, outdir='.'):
    template = templateFor('listener-tests.c')
    filename = serverNameFor(tree, 'listener-tests.c', outdir=outdir)
    writeTemplate(filename, template, tree)

def genTests(tree, outdir='.'):
    template = templateFor('tests.c')
    filename = clientNameFor(tree, 'connection-tests.c', outdir=outdir)
    writeTemplate(filename, template, tree)

def genServer(tree, outdir='.'):
    template = templateFor('listener.h')
    filename = serverNameFor(tree, 'listener.h', outdir=outdir)
    writeTemplate(filename, template, tree)

    template = templateFor('listener-lowlevel.h')
    filename = serverNameFor(tree, 'listener-lowlevel.h', outdir=outdir)
    writeTemplate(filename, template, tree)

    template = templateFor('listener.c')
    filename = serverNameFor(tree, 'listener.c', outdir=outdir)
    writeTemplate(filename, template, tree)

    template = templateFor('listener-closures.h')
    filename = serverNameFor(tree, 'listener-closures.h', outdir=outdir)
    writeTemplate(filename, template, tree)

def genShell(tree, outdir='.'):
    template = templateFor('shell.c')
    filename = clientNameFor(tree, 'shell.c', outdir=outdir)
    writeTemplate(filename, template, tree)

def genObject(tree, outdir='.'):
    for obj in tree.objects.values():
        if obj.singleton:
            continue
        template = templateFor('object.h')
        filename = clientNameFor(tree, obj.name.lower() + '.h', outdir=outdir)
        writeTemplate(filename, template, tree, Object=obj)
        template = templateFor('object.c')
        filename = clientNameFor(tree, obj.name.lower() + '.c', outdir=outdir)
        writeTemplate(filename, template, tree, Object=obj)

def writeFunc(name, type='void', args=[],
              static=False, inline=False,
              stream=None, header=False,
              ftypespace=0, ptypespace=0, rtypespace=0,
              prefix='', namespace=0, attrs=''):
    """
    ftypespace = spacing for func type
    ptypespace = spacing for param type
    rtypespace = spacing for dereferencing
    namespace = space for func name
    """
    if namespace:
        name = ('%%-%ds' % namespace) % name
    if ftypespace:
        ftypespace += 1
    stream.write(prefix)
    if static:
        stream.write('static ')
    if inline:
        stream.write('inline ')
    stream.write(type)
    if header:
        if ftypespace:
            stream.write(' ' * (ftypespace - len(type)))
        else:
            stream.write(' ')
    else:
        stream.write('\n')
        stream.write(prefix)
    stream.write(name)
    stream.write(' (')
    if not len(args):
        if attrs:
            attrs = ' ' + attrs
        if header:
            stream.write('void)%s;\n' % attrs)
        else:
            stream.write('void)%s\n' % attrs)
        return
    else:
        myArgs = []
        for t,n,d in args:
            n = ('*' * t.count('*')) + n
            t = t.replace('*', '')
            myArgs.append((t,n,d))
        ltype = max(ptypespace, max([len(t) for t,n,d in myArgs]))
        lname = max([len(n.replace('*','')) for t,n,d in myArgs])
        ldref = max(rtypespace, max([n.count('*') for t,n,d in myArgs]))
        #print '==========================================='
        #print ' Name..............: %s' % name
        #print ' Type..............: %s' % type
        #print ' Number Refs.......: %d' % ldref
        #print ' Longest type......: %d' % ltype
        #print ' Header?...........: %s' % header
        #print ' Prefix............: "%s"' % prefix
        #print ' Param Type Space..: %d' % ptypespace
        first = True
        i = 0
        for t,n,d in myArgs:
            i += 1
            if not first:
                stream.write(prefix)
                stream.write(' ' * len(name))
                stream.write('  ')
                if header:
                    if ftypespace:
                        stream.write(' ' * ftypespace)
                    else:
                        stream.write(' ' * (len(type) + 1))
            first = False
            stream.write(('%%-%ds' % ltype) % t)
            stream.write((' %%%ds' % ldref) % ('*' * n.count('*')))
            end = (i == len(myArgs)) and ')' or ','
            if header and end == ')':
                end += ';'
            if header:
                stream.write(n.replace('*',''))
                stream.write(end)
            else:
                stream.write(('%%-%ds' % (lname + 1)) % (n.replace('*','') + end))
                stream.write(' /* %s */' % d.upper())
            stream.write('\n')

def clientClassType(tree, name):
    return tree.clientPrefix + name

class Method(object):
    project = None

    def fullName(self):
        return '%s_%s' % (self.klass_lower.lower(), self.name.lower())

    def gtkdocize(self):
        if not self.doc:
            return ""
        return '\n * '.join(self.doc.strip().split('\n'))

    def writeVTable(self):
        if not self.vtable:
            raise ValueError, 'Method not overridable'
        name = '(*%s)' % self.name
        stream = StringIO.StringIO()
        writeFunc(name,
                  stream=stream,
                  type=self.type,
                  args=self.params,
                  attrs=self.attrs,
                  prefix='\t',
                  ftypespace=self.longType,
                  namespace=self.longName + 3,
                  rtypespace=self.longPtr,
                  ptypespace=self.longParamType,
                  header=True)
        stream.seek(0)
        return stream.read().strip()

    def writeHeader(self):
        name = '%s_%s' % (self.klass_lower, self.name)
        stream = StringIO.StringIO()
        writeFunc(name,
                  stream=stream,
                  type=self.type,
                  args=self.params,
                  attrs=self.attrs,
                  prefix='',
                  ftypespace=self.longType,
                  namespace=self.longName + len(self.klass) + 1,
                  rtypespace=self.longPtr,
                  ptypespace=self.longParamType,
                  header=True)
        stream.seek(0)
        return stream.read().strip()

    def writeMethod(self, static=False):
        name = '%s_%s' % (self.klass_lower, self.name)
        stream = StringIO.StringIO()
        writeFunc(name,
                  stream=stream,
                  type=self.type,
                  args=self.params,
                  static=static,
                  attrs=self.attrs)
        stream.seek(0)
        return stream.read().strip()

    def writeForward(self):
        name = '%s_%s' % (self.klass_lower, self.name)
        stream = StringIO.StringIO()
        writeFunc(name,
                  stream=stream,
                  type=self.type,
                  args=self.params,
                  ftypespace=self.longType,
                  namespace=self.longName + len(self.klass) + 1,
                  rtypespace=self.longPtr,
                  ptypespace=self.longParamType,
                  header=True)
        stream.seek(0)
        return stream.read().strip()

    def writeObject(self, header=False):
        name = '%s_%s' % (self.project.clientPrefix.lower(), self.name)
        stream = StringIO.StringIO()
        if self.name.endswith('_async'):
            args = self.params[2:]
            args.insert(0, ('%s%s*' % (self.project.clientPrefix, self.params[1][1].capitalize()), self.params[1][1], 'in'))
        elif self.name.endswith('_finish'):
            args = self.params[1:]
            args.insert(0, ('%s%s*' % (self.project.clientPrefix, self.object.name), self.object.name.lower(), 'in'))
        else:
            args = self.params[2:]
            args.insert(0, ('%s%s*' % (self.project.clientPrefix, self.object.name), self.object.name.lower(), 'in'))
        params = []
        skipNext = False
        for param in args:
            if skipNext:
                skipNext = False
                continue
            skipNext = False
            if param[1].capitalize() in self.project.objects:
                params.append((('%s%s*' % (self.project.clientPrefix, param[1].capitalize())) + (param[2] == 'out' and '*' or ''),
                               param[1], param[2]))
            elif param[1].capitalize()[:-1] in self.project.objects:
                params.append(('GList*' + (param[2] == 'out' and '*' or ''), param[1], param[2]))
                skipNext = True
            else:
                params.append(param)
        writeFunc(name,
                  stream=stream,
                  type=self.type,
                  args=params,
                  static=False,
                  header=header,
                  ftypespace=header and self.longType or 0,
                  namespace=header and self.longName or 0,
                  rtypespace=header and self.longPtr or 0,
                  ptypespace=header and self.longParamType or 0,
                  attrs=self.attrs)
        stream.seek(0)
        return stream.read().strip()

    def fullNameReplace(self, search, replace=''):
        return self.fullName().replace(search, replace)

    def nameReplace(self, search, replace=''):
        return self.name.replace(search, replace)

    def fullSpace(self):
        return ' ' * len(self.fullName())

    def isAsync(self):
        return self.name.endswith('_async')

    def isFinish(self):
        return self.name.endswith('_finish')

    def getFinish(self):
        if self.name.endswith('_async'):
            name = self.name[:-6] + '_finish'
            for m in self.project.methods['connection']:
                if m.name == name:
                    return m

    def dbusName(self):
        return ''.join([x.capitalize() for x in self.name.split('_')
                        if x not in ('async', 'finish', self.object.name.lower())])

    def serverName(self):
        return '%s_listener_%s' % (self.project.serverPrefix.lower(), self.name)

    def public(self):
        self.isPublic = True
        return self

def buildClientConn(tree):
    klassPtr = tree.clientPrefix + 'Connection*'
    klass = tree.clientPrefix + 'Connection'
    klass_lower = tree.clientPrefix.lower() + '_connection'

    def M(type, name, args=[], attrs='', doc=''): # method
        m = Method()
        m.project = tree
        m.klass_lower = klass_lower
        m.klass = klass
        m.static = False
        m.vtable = False
        m.doc = doc
        m.type = type
        m.name = name
        m.space = ' ' * len(name)
        m.attrs = attrs
        m.isPublic = True
        m.params = [(klassPtr, 'connection', 'in')]
        m.Prefix = tree.clientPrefix
        m.prefix = tree.clientPrefix.lower()
        m.PREFIX = tree.clientPrefix.upper()
        for arg in args:
            if len(arg) == 2:
                m.params.append((arg[0], arg[1], 'in'))
            elif len(arg) == 3:
                m.params.append(arg)
            else:
                raise TypeError, 'Arg must be 2 or 3 part tuple'
        return m

    def O(*a, **k): # overridable method
        m = M(*a, **k)
        m.vtable = True
        m.isPublic = False
        return m

    def S(*a, **k): # static methods
        m = M(*a, **k)
        del m.params[0]
        m.static = True
        m.vtable = False
        return m

    def A(*a, **k): # async methods
        m = O('void', *a, **k)
        m.params.extend([
            ('GCancellable*', 'cancellable', 'in'),
            ('GAsyncReadyCallback', 'callback', 'in'),
            ('gpointer', 'user_data', 'in'),
        ])
        m.name += '_async'
        m.space = ' ' * len(m.name)
        return m

    def F(*a, **k): # finish methods
        m = O('gboolean', *a, **k)
        m.params.insert(1, ('GAsyncResult*', 'result', 'in'))
        m.params.append(('GError**', 'error', 'out'))
        m.name += '_finish'
        m.space = ' ' * len(m.name)
        return m

    def Y(*a, **k):
        m = M('gboolean', *a, **k)
        m.params.append(('GError**', 'error', 'out'))
        m.vtable = False
        m.isPublic = False
        return m

    if not hasattr(tree, 'methods'):
        tree.methods = {}

    tree.methods['connection'] = [
        A('connect').public(),
        F('connect').public(),
        M('gboolean', 'connect', [('GError**', 'error', 'out')]),
        A('disconnect').public(),
        F('disconnect').public(),
        M('gboolean', 'disconnect', [('GError**', 'error', 'out')]),
        M('gboolean', 'is_connected'),
        S('GQuark', 'error_quark'),
        M('guint', 'hash'),
        S('GType', 'get_type', [], 'G_GNUC_CONST'),
        M('const gchar*', 'get_uri'),
        M('void', 'emit_state_changed', [(klass+'State', 'state')]),
        S(klassPtr, 'new_from_uri', [('const gchar*', 'uri')]),
    ]

    for obj in tree.objects.values():
        for rpc in obj.rpcs:
            b = []
            if not obj.singleton:
                b = [(obj.id, obj.name.lower(), 'in')]
            a = A(obj.name.lower() + '_' + rpc.name,
                  b + [p.toArg() for p in rpc.params if p.dir == 'in'],
                  doc=rpc.doc)
            f = F(obj.name.lower() + '_' + rpc.name,
                  [p.toArg() for p in rpc.params if p.dir == 'out'],
                  doc=rpc.doc)
            s = Y(obj.name.lower() + '_' + rpc.name,
                  b + [p.toArg() for p in rpc.params],
                  doc=rpc.doc)
            a.object = obj
            f.object = obj
            s.object = obj
            tree.methods['connection'].insert(0, s)
            tree.methods['connection'].insert(0, f)
            tree.methods['connection'].insert(0, a)

    # sort by name
    tree.methods['connection'].sort(lambda a,b: cmp(a.name, b.name))

    # cache longest name size
    r = 0
    n = 0
    p = 0
    t = 0
    for m in tree.methods['connection']:
        if len(m.type) > r:
            r = len(m.type)
        if len(m.name) > n:
            n = len(m.name)
        for param in m.params:
            if (param[0] + param[1]).count('*') > p:
                p = (param[0] + param[1]).count('*')
            if len(param[0]) > t:
                t = len(param[0])
    for m in tree.methods['connection']:
        m.longType = r
        m.longName = n
        m.longPtr = p
        m.longParamType = t
