#!/usr/bin/env python
#
# Copyright (c) 2010 Christian Hergert <chris@dronelabs.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

initialized = False
TYPE_INT, TYPE_PTR, TYPE_STRUCT_INT, TYPE_STRUCT_PTR = range(4)
registeredTypes = {}

paramConvTable = {
    'int': 'gint',
    'uint': 'guint',
    'uint8': 'guint8',
    'int64': 'gint64',
    'uint64': 'guint64',
    'long': 'glong',
    'ulong': 'gulong',
    'size': 'gsize',
    'ssize': 'gssize',
    'short': 'gshort',
    'ushort': 'gushort',
    'TimeVal': 'GTimeVal',
    'string': 'const gchar*',
    'string[]': 'gchar**',
    'bool': 'gboolean',
}

def registerTypes(types):
    for n,i in types:
        registeredTypes[n] = i

def initialize():
    registerTypes([
        ('int', TYPE_INT),
        ('uint', TYPE_INT),
        ('int64', TYPE_INT),
        ('uint64', TYPE_INT),
        ('long', TYPE_INT),
        ('ulong', TYPE_INT),
        ('short', TYPE_INT),
        ('ushort', TYPE_INT),
        ('TimeVal', TYPE_STRUCT_INT),
        ('string', TYPE_PTR),
        ('string[]', TYPE_PTR),
    ])
    global initialized
    initialized = True

def analyze(parseTree):
    if not initialized:
        initialize()

    # get all known local types
    localTypes = parseTree.objects.keys()
    localTypes.extend(parseTree.enums.keys())
    #print localTypes

    # convert licenses
    parseTree.clientLicense = parseTree.clientLicense.upper() + '.tmpl'
    parseTree.serverLicense = parseTree.serverLicense.upper() + '.tmpl'

    for obj in parseTree.objects.values():
        for prop in obj.props:
            if (prop.type not in localTypes) and (prop.type not in registeredTypes):
                raise Exception, 'Unknown type: %s' % prop.type
        if obj.id in paramConvTable:
            obj.id = paramConvTable[obj.id]
        for rpc in obj.rpcs:
            rpc.full_name = obj.name.lower() + '_' + rpc.name.lower()
            i = 0
            for param in rpc.params:
                i += 1
                # convert to language later
                if param.type in paramConvTable:
                    param.type = paramConvTable[param.type]
                if param.dir == 'out':
                    param.type = param.type.replace('const ', '')
                if param.type.endswith('[]'):
                    # expand and add len if needed
                    t = param.type.replace('[]', '')
                    if t not in localTypes and t not in paramConvTable:
                        raise TypeError, 'Unknown type %s' % t
                    if t in localTypes:
                        t = parseTree.objects[t]
                        if t.id in paramConvTable:
                            t.id = paramConvTable[t.id]
                        if t.id in ('gint',):
                            p = param.__class__()
                            p.name = param.name + '_len'
                            p.dir = param.dir
                            p.type = 'gsize'
                            rpc.params.insert(i, p)
                            i += 1
                        elif t.id in ('const gchar*',):
                            pass
                        else:
                            raise TypeError, 'Unknown indexer type %s' % t.id
                        param.type = t.id + '*'
                    else:
                        p = param.__class__()
                        p.name = param.name + '_len'
                        p.dir = param.dir
                        p.type = 'gsize'
                        rpc.params.insert(i, p)
                        i += 1
                        param.type = paramConvTable[t] + '*'
                if param.type in parseTree.enums:
                    param.type = 'gint'
                elif param.type in localTypes:
                    t = parseTree.objects[param.type]
                    if not hasattr(t, 'id'):
                        raise TypeError, 'Type %s does not specify indexer' % t.name
                    if t.id in paramConvTable:
                        param.type = paramConvTable[t.id]
                    else:
                        param.type = t.id
                if param.dir == 'out':
                    param.type += '*'

    # check dbus stuff
    if not hasattr(parseTree, 'dbusname'):
        parseTree.dbusname = 'org.gnome.' + parseTree.name
    if not hasattr(parseTree, 'dbuspath'):
        parseTree.dbuspath = '/' + parseTree.dbusname.replace('.', '/')
