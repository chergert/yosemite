#!/usr/bin/env python
#
# Copyright (c) 2010 Christian Hergert <chris@dronelabs.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
Yosemite is a RPC generator for various RPC subsystems.
"""

import os
import sys
import getopt

def usage(argv=sys.argv, stream=sys.stdout):
    """
    Print application usage to @stream.
    """
    name = len(argv) and argv[0] or 'yosemite'
    print >> stream, 'usage:'
    print >> stream, '  %s [OPTIONS] NAME FILENAME' % name
    print >> stream, ''
    print >> stream, 'synopsis:'
    print >> stream, '  yosemite is a rpc generator'
    print >> stream, ''
    print >> stream, 'options:'
    print >> stream, '  -h, --help            Show this help menu'
    print >> stream, '      --debug           Debug yosemite'
    print >> stream, '  -c, --client          Generate client'
    print >> stream, '  -s, --server          Generate server'
    print >> stream, '      --outdir=         Output directory for client and server'
    print >> stream, '      --client-outdir=  Output directory for client'
    print >> stream, '      --server-outdir=  Output directory for server'
    print >> stream, '      --dbus            Generate DBus backend'
    print >> stream, '      --tests           Generate client tests'
    print >> stream, '      --object          Generate object model'
    # Hidden feature
    #print >> stream, '      --shell           Generate a client interactive shell'
    print >> stream, ''
    print >> stream, 'examples:'
    print >> stream, '  yosemite.py --outdir=outdir --client --server --dbus --tests --object Perfkit sample.rpc'
    print >> stream, ''

def main(argv=sys.argv, stdout=sys.stdout, stderr=sys.stderr):
    """
    Application entry point for Yosemite.
    """
    try:
        sOpts = 'hcs'
        lOpts = ['help', 'client', 'server', 'dbus', 'debug',
                 'client-outdir=', 'server-outdir=', 'tests',
                 'outdir=', 'shell', 'object']
        opts, args = getopt.getopt(argv[1:], sOpts, lOpts)
    except getopt.GetoptError, ex:
        print >> stderr, str(ex)
        return 1

    doClient = False
    doServer = False
    doDBus = False
    doTests = False
    dirClient = '.'
    dirServer = '.'
    doDebug = False
    doShell = False
    doObject = False

    for o,a in opts:
        if o in ('-h', '--help'):
            usage(argv=argv, stream=stdout)
            return 0
        if o in ('-c', '--client'):
            doClient = True
        if o in ('-s', '--server'):
            doServer = True
        if o in ('--dbus',):
            doDBus = True
        if o in ('--client-outdir',):
            dirClient = a
        if o in ('--server-outdir',):
            dirServer = a
        if o in ('--outdir',):
            dirServer = a
            dirClient = a
        if o in ('--debug',):
            doDebug = True
        if o in ('--tests',):
            doTests = True
        if o in ('--shell',):
            doShell = True
        if o in ('--object',):
            doObject = True

    if len(args) != 2:
        usage(stream=stderr)
        return 1
    name, filename, = args

    import parser
    import analyzer
    import log
    import codegen

    log.setStreams(stdout=stdout, stderr=stderr)
    parseTree = parser.parse(filename, debug=doDebug)
    parseTree.name = name
    analyzer.analyze(parseTree)
    if doClient:
        print >> stderr, 'Generating client ...'
        codegen.genClient(parseTree, outdir=dirClient)
        if doDBus:
            codegen.genClientDBus(parseTree, outdir=dirClient)
        if doTests:
            codegen.genTests(parseTree, outdir=dirClient)
        if doShell:
            codegen.genShell(parseTree, outdir=dirClient)
        if doObject:
            codegen.genObject(parseTree, outdir=dirClient)
    if doServer:
        print >> stderr, 'Generating server ...'
        codegen.genServer(parseTree, outdir=dirServer)
        if doDBus:
            codegen.genServerDBus(parseTree, outdir=dirClient)
        if doTests:
            codegen.genServerTests(parseTree, outdir=dirServer)

    return 0

if __name__ == '__main__':
    sys.exit(main())
