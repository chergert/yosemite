#!/usr/bin/env python
#
# Copyright (c) 2010 Christian Hergert <chris@dronelabs.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import errors

def getReader(filename):
    return Reader(file(filename).read())

def parse(filename, debug=False):
    tree = ParseTree()
    reader = getReader(filename)
    cmd, cmdReader = reader.next()
    while cmd:
        #print 'cmd', cmd, cmdReader
        if cmd == 'license':
            if cmdReader.direction == 'client':
                tree.clientLicense = cmdReader.license
            else:
                tree.serverLicense = cmdReader.license
        elif cmd == 'copyright':
            tree.copyright = cmdReader.copyright
        elif cmd == 'prefix':
            if cmdReader.direction == 'client':
                tree.clientPrefix = cmdReader.prefix
            else:
                tree.serverPrefix = cmdReader.prefix
        elif cmd == 'enum':
            tree.enums[cmdReader.name] = cmdReader.enums
        elif cmd == 'object':
            tree.objects[cmdReader.object.name] = cmdReader.object
        elif cmd == 'dbus':
            setattr(tree, 'dbus' + cmdReader.key, cmdReader.value)
        else:
            print 'Unknown command', cmd
        #print reader.pos, 'of', reader.len
        cmd, cmdReader = reader.next()

    if not debug:
        return tree

    for name,obj in tree.objects.iteritems():
        print 'Object', name
        for prop in obj.props:
            for line in rpc.doc.strip().split('\n'):
                print '\t# %s' % line
            print '\tprop %s %s' % (prop.type, prop.name)
            print ''
        for rpc in obj.rpcs:
            for line in rpc.doc.strip().split('\n'):
                print '\t# %s' % line
            print '\trpc %s (%s)' % (rpc.name, ', '.join(['%s %s %s' % (x.dir, x.type, x.name) for x in rpc.params]) or 'void')
            print ''
    return tree

class Base(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

class Enum(Base):
    name = ''
    value = ''
    doc = ''

class Object(Base):
    name = ''
    id = None
    singleton = False
    def __init__(self, **kwargs):
        self.rpcs = []
        self.signals = []
        self.props = []
        self.__dict__.update(kwargs)

class Prop(Base):
    name = ''
    type = 'int'
    doc = ''

class Param(Base):
    name = ''
    dir = 'in'
    type = 'int'
    def toArg(self):
        t = self.type
        if self.dir == 'out':
            t = t.replace('const ', '')
        return (t, self.name, self.dir)

class Signal(Base):
    name = ''
    params = None
    doc = ''
    def __init__(self, **kwargs):
        self.params = []
        self.__dict__.update(kwargs)

class Rpc(Base):
    def __init__(self, **kwargs):
        self.params = []
        self.__dict__.update(kwargs)

class ParseTree(object):
    copyright = '2010 Christian Hergert <chris@dronelabs.com>'
    clientLicense = 'MITX11'
    serverLicense = 'MITX11'
    clientPrefix = 'Rpc'
    serverPrefix = 'Rpc'
    enums = {}
    objects = {}

class LicenseReader(object):
    def __init__(self, line):
        try:
            self.direction, self.license = line.split()
            self.license.strip()
        except:
            self.license = line.strip()

class KeyValueReader(object):
    def __init__(self, line):
        self.key, self.value = line.split()

class CopyrightReader(object):
    def __init__(self, line):
        self.copyright = line.strip()

class PrefixReader(object):
    def __init__(self, line):
        self.direction, self.prefix = line.split()

class EnumReader(object):
    def __init__(self, name, data):
        #print '>>>>>>%s<<<<<<' % data
        curdoc = ''
        inDoc = False
        self.name = name.strip()
        self.enums = []
        for line in data.split('\n'):
            if line.strip() == '"""':
                inDoc = not inDoc
                continue
            if inDoc:
                curdoc += '\n' + line.strip()
                continue
            parts = [a.strip() for a in line.split('=')]
            if not parts[0].strip():
                continue
            if len(parts) == 1:
                self.enums.append(Enum(name=parts[0], doc=curdoc))
            else:
                self.enums.append(Enum(name=parts[0], value=parts[1], doc=curdoc))
            curdoc = ''
        #print '%s\n%s' % (self.name, '\n'.join(['"%s = %s"' % (x.name, x.value) for x in self.enums]))

class ObjectReader(object):
    def __init__(self, name, data):
        self.object = Object()
        self.object.name = name.strip()
        curdoc = ''
        inDoc = False
        for line in data.split('\n'):
            if line.strip() == '"""':
                inDoc = not inDoc
                continue
            if inDoc:
                curdoc += '\n' + line.strip()
                continue
            line = line.strip()
            if line.startswith('rpc'):
                self.readRpc(line, curdoc)
                curdoc = ''
            elif line.startswith('prop'):
                self.readProp(line, curdoc)
                curdoc = ''
            elif line.startswith('id'):
                self.readId(line)
                curdoc = ''
            elif line.startswith('signal'):
                self.readSignal(line, curdoc)
                curdoc = ''
            elif line.startswith('singleton'):
                self.readSingleton(line)
                curdoc = ''

    def readRpc(self, line, doc=''):
        rdr = Reader(line)
        assert rdr.readWord() == 'rpc'
        name = rdr.readWord().lower()
        params = []
        if '(' in rdr.data[rdr.pos:]:
            rdr.readToAny('(')
        data = rdr.readToAny(')')
        for p in data.split(','):
            parts = p.split()
            if not parts:
                continue
            elif parts[0] in ('in', 'out'):
                dir = parts[0]
                type = parts[1]
                pname = parts[2]
            else:
                dir = 'in'
                type = parts[0]
                pname = parts[1]
            params.append(Param(dir=dir, type=type, name=pname))
        rpc = Rpc(name=name, params=params, doc=doc)
        self.object.rpcs.append(rpc)

    def readProp(self, line, doc=''):
        line = line.strip()
        _, t, n = line.split()
        self.object.props.append(Prop(name=n, type=t, doc=doc))

    def readId(self, line):
        self.object.id = line.strip().split()[1]

    def readSignal(self, line, doc=''):
        pass

    def readSingleton(self, line):
        self.object.singleton = True

class Reader(object):
    def __init__(self, data):
        self.data = data
        self.pos = 0
        self.len = len(data)

    def next(self):
        #print self.pos, '"""%s<<<' % self.data[self.pos:self.pos+40]
        if self.pos >= len(self.data):
            return None
        if self.pos and self.data[self.pos-1] != '\n':
            self.readToEol()
        command = self.readWord()
        while self.pos < self.len:
            if command == 'license':
                return command, LicenseReader(self.readToEol())
            elif command == 'copyright':
                return command, CopyrightReader(self.readToEol())
            elif command == 'prefix':
                return command, PrefixReader(self.readToEol())
            elif command == 'enum':
                name = self.readWord()
                self.readToOpenParen()
                return command, EnumReader(name, self.readToCloseParen())
            elif command == 'object':
                name = self.readWord()
                self.readToOpenParen()
                return command, ObjectReader(name, self.readToCloseParen())
            elif command == 'dbus':
                return command, KeyValueReader(self.readToEol())
            self.readToEol()
            command = self.readWord()
        return None, None

    def moveToWord(self):
        for i in xrange(self.pos, self.len):
            if self.data[i] in ('\t', '\n', ' '):
                self.pos += 1
            else:
                return

    def readWord(self):
        self.moveToWord()
        return self.readToAny('\t', '\n', ' ', '(')

    def readToEol(self):
        return self.readToAny('\n')

    def readToOpenParen(self):
        return self.readToAny('{')

    def readToCloseParen(self):
        return self.readToAny('}')

    def readToAny(self, *chars):
        start = self.pos
        for i in xrange(self.pos, self.len):
            if self.data[i] in chars:
                self.pos = i + 1
                #print '!!!!!'
                #print self.data[start:i]
                #print '____'
                return self.data[start:i]
        self.pos = self.len
        return self.data[start:]
